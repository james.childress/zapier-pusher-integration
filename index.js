const authentication = require('./authentication');
const publishEventCreate = require('./creates/publish_event.js');

module.exports = {
  version: require('./package.json').version,
  platformVersion: require('zapier-platform-core').version,
  authentication: authentication,
  creates: { [publishEventCreate.key]: publishEventCreate },
};
