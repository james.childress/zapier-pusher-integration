const { init, publish } = require('./utils/pusher');

const TEST_EVENT = {
  channel: 'zapier-authentication',
  event: 'test',
};

const perform = async (_, { authData }) => {
  const pusher = init(authData);
  const json = await publish(pusher, TEST_EVENT);
  return json;
};

module.exports = {
  type: 'custom',
  test: perform,
  connectionLabel: 'App ID: {{app_id}}',
  fields: [
    {
      computed: false,
      key: 'info',
      required: false,
      label: 'Info',
      type: 'copy',
      helpText:
        'All of these values can be found for your Pusher app at `https://dashboard.pusher.com/apps/YOUR_APP_ID/keys`. \n\n To verify that your credentials are correct, a `test` event will be sent to the channel `zapier-authentication`.',
    },
    {
      computed: false,
      key: 'app_id',
      required: true,
      label: 'App ID',
      type: 'string',
    },
    {
      computed: false,
      key: 'key',
      required: true,
      label: 'Key',
      type: 'string',
    },
    {
      computed: false,
      key: 'secret',
      required: true,
      type: 'string',
      label: 'Secret',
    },
    {
      computed: false,
      key: 'cluster',
      required: true,
      label: 'Cluster',
      type: 'string',
    },
  ],
};
