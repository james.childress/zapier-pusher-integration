const Pusher = require('pusher');

const init = (authData) =>
  new Pusher({
    appId: authData.app_id,
    key: authData.key,
    secret: authData.secret,
    cluster: authData.cluster,
    useTLS: true,
  });

const publish = async (pusher, inputData) => {
  const response = await pusher.trigger(
    inputData.channel,
    inputData.event,
    inputData.data || {}
  );
  const json = await response.json();
  return json;
};

module.exports = { init, publish };
