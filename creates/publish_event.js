const { init, publish } = require('../utils/pusher');

const sample = { success: true };

const perform = async (_, { authData, inputData }) => {
  const pusher = init(authData);
  await publish(pusher, inputData);
  return sample;
};

module.exports = {
  operation: {
    perform,
    sample,
    inputFields: [
      {
        key: 'channel',
        label: 'Channel',
        type: 'string',
        required: true,
        list: false,
        altersDynamicFields: false,
      },
      {
        key: 'event',
        label: 'Event',
        type: 'string',
        required: true,
        list: false,
        altersDynamicFields: false,
      },
      {
        key: 'data',
        label: 'Data',
        dict: true,
        required: false,
        altersDynamicFields: false,
      },
    ],
  },
  key: 'publish_event',
  noun: 'Event',
  display: {
    label: 'Publish Event',
    description: 'Sends an event to a Pusher channel',
    hidden: false,
    important: true,
  },
};
