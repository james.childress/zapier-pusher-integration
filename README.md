# Pusher integration for Zapier

A [Zapier](http://zapier.com) integration for sending [Pusher](http://pusher.com) events.

### Authentication

To authenticate with Pusher, the following values from your auth token are required.
These values can be found at `https://dashboard.pusher.com/apps/<APP_ID>/keys`.

- `app_id`
- `key`
- `secret`
- `cluster`

To verify that your credentials are correct, Zapier will occasionally send a `test` event
to the `zapier-authentication` channel of your Pusher app.

### Actions

**Publish Event**: Currently, this integration only exposes a single `create` action, named "Publish Event".
This action sends a single event to a channel. You may optionally specify additional [data](https://pusher.com/docs/channels/server_api/http-api/#data-2018448251) to include with the event.

### Triggers

Although this integration does not provide any triggers, it is possible to trigger a Zap from a Pusher event. First, create a Zap with a [Webhooks by Zapier](https://zapier.com/apps/webhook/integrations) trigger. Then, configure a webhook for your Pusher app at `https://dashboard.pusher.com/apps/<APP_ID>/webhooks`.
